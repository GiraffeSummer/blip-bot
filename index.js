//Recycled bot from bademo which is made for funhouserestart and the serverlist
const auth = require("./auth.json");
const fs = require('fs');

const token = process.env.token || auth.token;
const restart = auth.restartServer;
const serverApi = auth.serverApi;

const discord = require('discord.js');
const Prefix = "!";

const admins = [
    "151039550234296320",
    "258720417005436929"
]
const WelcomeMsg = "Welcome to the RFMP Discord server, please make sure to read <#454468006056296458> and <#454467960241782785>";


var loop;
var loop2;

// initialize client 
const client = new discord.Client();

function CheckServers() {
    httpGetAsync(serverApi, callback);

    function callback(body) {
        console.log("ping")
        if (body.success == false) {
            clearInterval(loop);
            loop2 = setInterval(CheckServers, 86400000) //1 day
            let chanId = "569694145426489344";
            let channel = client.channels.get(chanId);

            if (channel.lastMessageID !== "575724556094799892" && channel.lastMessageID !== undefined) {
                channel.send(`(Api offline, Sorry!)`);
            }

            console.log("API offline stopping requests.");
            return;
        }
        else {
            clearInterval(loop2);
            let chanId = "569694145426489344";
            let channel = client.channels.get(chanId);
            //channel.send(`test`);

            if (channel.lastMessageID !== "575724556094799892" && channel.lastMessageID !== undefined) {
                channel.fetchMessage(channel.lastMessageID)
                    .then(message => message.delete())
                    .catch(function (e) { })

                console.log("Working again!");
            }
        }

        var servers = body.data;
        var emb =
        {
            embed: {
                title: `Servers Online: (version: ${servers.version})`,
                description: servers.amount,
                thumbnail: {
                    url: "https://cdn.discordapp.com/avatars/497490017506164736/471acb1d3c7e2f6e7569b918e032f76b.png?size=1024"
                },
                color: 1698076,
                fields: []
            }
        }

        //max 25 fields, probably want to filter least populated ones, or password protected ones
        let amount = (servers.servers.length > 25) ? 25 : servers.servers.length;
        for (let index = 0; index < amount; index++) {
            let pass = "";
            if (servers.servers[index].password)
                pass = ":lock: "

            var f = {
                name: pass + `**${servers.servers[index].name}**`,
                value: `${servers.servers[index].players}/${servers.servers[index].maxPlayers} Players`
            }

            emb.embed.fields.push(f);
        }
        let msId = "575724556094799892";
        let chanId = "569694145426489344";

        let channel = client.channels.get(chanId);
        channel.fetchMessage(msId)
            .then(message => message.edit(emb))
    }
}

client.on('guildMemberAdd', member => {
    //! on join sends them the contents of WelcomeMsg
    member.send(WelcomeMsg).catch(err => {
        console.log("attempted to send a message to an unkown user")
    });
});


// runs when a message is sent
client.on('message', message => {
    if (message.author.bot || message.guild == null) {
        //! stops bot from being able to spam it
        //! stop dm's from breaking it
        return;
    }

    //logs command into console
    if (message.content.startsWith(Prefix)) console.log(`[${message.channel.name}][${message.author.username}]` + message.content);

    //ellipsis bruh counter
    if (message.author.id == "418722343741816855") { //if ellipsis
        if (message.content.toLowerCase().startsWith("?bruh") || message.content.toLowerCase().startsWith("!bruh")) {
            let counter = LoadJson("./wordcount.json")
            message.channel.send({
                embed: {
                    color: 0x7FFF00,
                    description: `**You said "bruh" or "bru" ${counter[message.author.id]} times** (since the counter was added to the bot)`
                }
            })
        }
        else if (message.content.toLowerCase().includes("bru") || message.content.toLowerCase().includes("bruh")) {
            //id: 418722343741816855
            //file: wordcount.json
            console.log("BRUHH");

            WriteWordCount({
                color: 0x7FFF00,
                description: `**${message.author.tag}** Said bruh again `
            })
        }
    }

    async function SendInvite(message) {
        let invite = await client.channels.get("501178569398157315").createInvite({
            maxAge: 0,
            maxUses: 1
        }, `Requested with command by ${message.author.tag}`).catch(console.log);

        message.author.send(invite ? `:tada:**Congratulations**:tada: on your **420th bruh moment!**\nHere is your invite to a special server...
        ${invite}` : "There has been an error during the creation of the invite. (say bruh again)");
    }

    if (message.author.id == "223903236069785601") { //if owez
        if (message.content.toLowerCase().includes("python") ||
            message.content.toLowerCase().includes("rust") ||
            message.content.toLowerCase().includes("i just made") ||
            message.content.toLowerCase().includes("i made") ||
            message.content.toLowerCase().includes("i have made") ||
            message.content.toLowerCase().includes("i wrote") ||
            message.content.toLowerCase().includes("i just wrote") ||
            message.content.toLowerCase().includes("gitlab.")
        ) {
            //id: 223903236069785601
            //file: wordcount.json
            console.log("brag owez");

            WriteWordCount({
                color: 0xFF0000,
                description: `**${message.author.tag}** Bragged about programming \n${message.content}`
            });
        }
    }

    function WriteWordCount(em) {
        let counter = LoadJson("./wordcount.json")
        if (counter[message.author.id] == undefined)
            counter[message.author.id] = 1;
        else
            counter[message.author.id]++;
        SaveJson(counter, "./wordcount.json")
        em.description += `(total: **${counter[message.author.id]}**)`;

        client.channels.get("501527005351772171").send({
            embed: em
        })

        if (message.author.id == "418722343741816855") {
            if (counter == 420) {
                SendInvite(message)

                let emb = {
                    description : `:tada:**GOAL REACHED**:tada:\nSent Invite to ${message.author.tag}`,
                    color: 0x7FFF00,
                }
                client.channels.get("501527005351772171").send({
                    embed: emb
                })
            }
        }
    }

    //restart bot command
    if (message.content.toLowerCase().startsWith(Prefix + 'restart') && admins.includes(message.author.id)) {
        console.log("Restart");
        message.delete().catch(function (e) { e });
        setTimeout(function () {
            process.exit();
        }, 1000);
    }

    // !suggest command
    if (message.content.toLowerCase().startsWith(Prefix + "suggest")) {

        let args = message.content.substring(Prefix.length + "suggest ".length); // get's the suggestion

        if (args === "") return;    //! if no suggestion do nothing

        client.channels.get("499364414861082625").send(message.author.tag + " suggests: " + args).then(sentmsg => {
            //! sends suggestion and then reacts with a thumbs up and thumbs down
            sentmsg.react("👍").then(() => {
                //! reacts to message with :thumsup: and :thumbsdown:
                sentmsg.react("👎");
            })

            message.channel.send("sent your suggestion successfully!"); // shows that it sent the suggestion
        });

        client.channels.get("501527005351772171").send(message.author.tag + " suggests: " + args) // nothing here
    }


    //debug command
    if (message.content.toLowerCase().startsWith(Prefix + 'ping') && admins.includes(message.author.id)) {
        message.reply("Pong!")
    }


    //!funhouserestart
    if (message.content.toLowerCase().startsWith(Prefix + 'funhouserestart')) { //restarts cripple rick's server
        //! !funhouserestart command

        let author = message.guild.member(message.author)
        author.roles.find(x => x.id === '560600488123564047')
        //author.roles.find('id', '560600488123564047')
        if (author.roles.find(x => x.id === '560600488123564047')) {
            httpGetAsync(restart, callback);

            function callback(body) {
                if (body.data.error === null) {
                    message.channel.send(`**${message.author.username}** Restarted the server!`)
                }
                else {
                    message.channel.send(`Something went wrong! got error ${body.error}`)
                }
            }
        }
        else {
            message.reply("You cannot restart the server!");
        }
    }
});


client.on('ready', () => {
    //! on ready function
    console.log(`blippo is online! (${client.user.tag})`);

    if (false /* Not required for steam version*/) {
        loop = setInterval(CheckServers, 60000);
        CheckServers();
    }
});

client.login(token).catch(err => {
    //! login to the client
    console.error("failed to login!")
})

function httpGetAsync(url, callback, method) {
    const https = method || require('https');

    https.get(url, (resp) => {
        let data = '';

        resp.on('data', (chunk) => {
            data += chunk;
        });

        resp.on('end', () => {
            let resp = {
                success: !data.includes("<!DOCTYPE html>"),
                data: {}
            }
            if (!data.includes("<!DOCTYPE html>")) {
                resp.data = JSON.parse(data)
                resp.success = true;
            }
            callback(resp)
        });

    }).on("error", (err) => {
        console.log("Error: " + err.message);
    });
}

function SaveJson(json, location) {
    let data;
    if (!fs.existsSync(location)) { var newData = {}; fs.writeFileSync(location, newData); }
    data = JSON.stringify(json, null, 4);
    fs.writeFileSync(location, data);
}

function LoadJson(location) {
    let rawdata = fs.readFileSync(location);
    let loadData = JSON.parse(rawdata);
    return loadData;
}