/*

    Blip-Bot
    Copyright (C) 2018  bademo and aBigPickle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

const discord = require('discord.js')
// the people who are on the team
let eagles = [];
let ravens = [];
// the current map
let map = "No Map!";
// scores for eagle and raven
let scoreEagle = 0;
let scoreRaven = 0;
// the discord client
let client;

// exports as master function
// this handles !team
module.exports = function (message) {
    let messageArray = message.content.toLowerCase().split(" "); // breaks arguments into seperate strings

    if (messageArray.length === 1) {
        //! makes sure there's an argument
        message.channel.send("Please input an argument!");

        return;
    }

    let args_1 = messageArray.slice(1); // get's first argument

    if (args_1[0] === 'help') {
        //! basic help command
        message.channel.send("**Commands:**\n\n!team help: *this menu!*\n!team list: *shows the team list.*\n!team add <user> <team>: *adds a user to a team (eagle or raven).* (ADMINISTRATOR)\n!team remove <user>: *removes a user from the teamlist.* (ADMINISTRATOR)\n!team reset: *resets team list.* (ADMINISTRATOR)\n!team setmap: *sets the current playing map.* (ADMINISTRATOR)\n!team swap: *swaps the two teams* (ADMINISTRATOR)\n!team setscore <eagle> <raven>: *sets the scores.* (ADMINISTRATOR)");

        return;
    }

    if (args_1[0] === 'add') {
        //! adds a member
        if (!message.member.hasPermission("ADMINISTRATOR")) {
            //! makes sure they're an admin

            return message.channel.send("You do not have permission to use this command!");
        }

        if (args_1.length < 2) {
            //! makes sure there's a name
            message.channel.send("Please specify a name!");

            return;
        }

        var user = args_1[1];

        if (args_1.length < 3) {
            //! makes sure there's a team
            message.channel.send("Please specify a team!");

            return;
        }

        if (args_1[2].toLowerCase() === 'eagle') {
            //! checks what team they put
            eagles.push(user);
            message.channel.send(user + " was added to eagles succesfully!");

            return;
        }

        if (args_1[2].toLowerCase() === 'raven') {
            ravens.push(user);
            message.channel.send(user + " was added to ravens succesfully!");

            return;
        }
        else {
            //! in case team is incorrect (possible todo is expand accepted arguments)
            message.channel.send("Unknown team! Please use either \'raven\' or \'eagle\'.");

            return;
        }
    }
    if (args_1[0].toLowerCase() === 'remove') {
        if (!message.member.hasPermission("ADMINISTRATOR")) {

            return message.channel.send("You do not have permission to use this command!");
        }

        if (args_1.length < 2) {
            //! makes sure there's a name
            message.channel.send("Please specify a name!");

            return;
        }

        let user = args_1[1];

        if (eagles.includes(user)) {
            //! moves users
            let pos = eagles.indexOf(user);
            eagles.splice(pos, 1);
            message.channel.send("Removed " + user + " from eagles succesfully!");

            return;
        }

        if (ravens.includes(user)) {
            let posraven = ravens.indexOf(user);
            ravens.splice(posraven, 1);
            message.channel.send("Removed " + user + " from ravens succesfully!");

            return;
        }
        else {
            message.channel.send("Could not find " + user + " in any team!");

            return;
        }
    }

    if (args_1[0] === 'reset') {
        //! resets team list
        if (!message.member.hasPermission("ADMINISTRATOR")) {
            //! makes sure they have admin
            return message.channel.send("You do not have permission to use this command!");
        }

        eagles = [];
        ravens = [];
        scoreEagle = 0;
        scoreRaven = 0;
        map = "No Map!";
        message.channel.send("Teams cleared and reset!");

        return;
    }


    if (args_1[0] === 'setmap') {
        //! changes map
        if (!message.member.hasPermission("ADMINISTRATOR")) {
            //! makes sure they have admin
            return message.channel.send("You do not have permission to use this command!");
        }

        if (args_1.length < 2) {
            //! makes sure there's enough args
            return message.channel.send("Please specify a map!");
        }

        // changes map
        map = args_1[1];
        message.channel.send("Map changed to " + args_1[1] + "!");

        return;
    }

    if (args_1[0] === 'swap') {
        //! swaps teams
        if (!message.member.hasPermission("ADMINISTRATOR")) {
            return message.channel.send("You do not have permission to use this command!");
        }

        let ravenCopy = ravens.slice();
        ravens = eagles.slice();
        eagles = ravenCopy.slice();
        message.channel.send("Teams swapped!");

        return;
    }

    if (args_1[0] === 'list') {
        //! shows info (possibly change to !teams info)
        let eagleExists = "";
        let ravenExists = "";

        if (eagles.length < 1) {
            eagleExists = "No Members.";
        }

        if (ravens.length < 1) {
            ravenExists = "No Members.";
        }

        const embed = new discord.RichEmbed()
            .setAuthor(message.author.username, message.author.avatarURL)
            .setDescription("\'!team reset\' to clear teams.")
            .setColor(0x00AE86)
            .setTitle("Current Team List")
            .addField("Map: ", map)
            .addField("Score: ", scoreEagle + " - " + scoreRaven)
            .addField("Eagles: ", eagles.join('\n') + eagleExists, true)
            .addField("Ravens: ", ravens.join('\n') + ravenExists, true)
            .setTimestamp()
            .setFooter(client.user.username, client.user.avatarURL);
        message.channel.send(embed);

        return;
    }

    if (args_1[0] === 'setscore') {
        //! sets the score
        if (!message.member.hasPermission("ADMINISTRATOR")) {
            return message.channel.send("You do not have permission to use this command!");
        }

        if (args_1.length < 3) {
            message.channel.send("Please specify a full score!");

            return;
        }

        scoreEagle = args_1[1];
        scoreRaven = args_1[2];
        message.channel.send("Set scores to: " + args_1[1] + " - " + args_1[2] + ".");

        return;
    }
    else {
        //! unspecified param
        message.channel.send("Unknown command.");
    }
}

module.exports.init = function (Client) {
    client = Client
}
